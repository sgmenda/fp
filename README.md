# Browser Fingerprinting Experiments

Some fingerprinting experiments.

- [Inferring Viewport Size Using Just CSS](/css-viewport-size.html)
- [Support for Common Video MIME Types](/support-for-common-video-mime-types.html)
